package main

import (
	"testing"
)

func TestTask(t *testing.T) {
	n, k := 2, 1
	d := n * 2 - 1
	m := make([][]int, d)
	test := "5 4 7 8 9 6 3 2 1"

	for i := range m {
		m[i] = make([]int, d)
	}

	for i := range m {
		for j := range m[i] {
			m[i][j] = k
			k++
		}
	}

	if do(m) != test {
		t.Error("Invalid result")
	}
}