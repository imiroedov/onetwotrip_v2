package main

import (
	"fmt"
	"flag"
	"time"
	"strings"
	"math/rand"
)

var n = flag.Int("n", 2, "Dimension matrix is [2n-1 x 2n-1]")

func main() {
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())

	d := *n * 2 - 1
	m := make([][]int, d)

	for i := range m {
		m[i] = make([]int, d)
	}

	for i := range m {
		for j := range m[i] {
			m[i][j] = rand.Intn(d * d)
		}
	}

	fmt.Println("INPUT:")
	for i := range m {
		fmt.Println(m[i])
	}

	fmt.Println("OUTPUT:")
	fmt.Println(do(m))
}

func do(matrix [][]int) string {
	s := []string{}
	n := len(matrix)
	x, y := n / 2, n / 2

	for i := 2; i <= 2 * n; i++ {
		for j := 0; j < i / 2; j++ {
			s = append(s, fmt.Sprintf("%d", matrix[y][x]))
			x += ((i - 1) % 4 - 2) % 2
			y += ((i - 1) % 4 - 1) % 2
		}
	}

	return strings.Join(s, " ")
}